import * as React from 'react';
import { IHelloWorldProps } from './IHelloWorldProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { Checkbox, ICheckboxProps } from 'office-ui-fabric-react/lib/Checkbox';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import styles from './HelloWorld.module.scss';

interface IFormState {
  naBroadcastEntities?: boolean;
  broadcastEntities?: string;
  naPlainBroadcastEntities?: boolean;
  plainBroadcastEntities?: string;
}


export default class HelloWorld extends React.Component<IHelloWorldProps, IFormState> {

  constructor(props: IHelloWorldProps) {
    super(props);
    this.state = {
      naBroadcastEntities: false,
      broadcastEntities: "",
      naPlainBroadcastEntities: false,
      plainBroadcastEntities: "",
    };
    this._onCheckBoxChange = this._onCheckBoxChange.bind(this);
    this._onTextBoxChange = this._onTextBoxChange.bind(this);
    this._onPlainCheckBoxChange = this._onPlainCheckBoxChange.bind(this);
  }

  public async componentDidMount() {
    // form load
    this.setState({
      naBroadcastEntities: true,
      broadcastEntities: "NA",
      naPlainBroadcastEntities: true,
      plainBroadcastEntities: "NA"
    
    });
  }

  private _onTextBoxChange(ev: any) {
    const target: HTMLInputElement = ev.target;
    const name: string = target.name;
    this.setState({
      [name]: target.value
    });
  }

  private _onCheckBoxChange(ev: any, isChecked: boolean) {
    const target: HTMLInputElement = ev.target;
    const name: string = target.name;
    const field: string = target.dataset.field;
    console.info(`set ${name} checked value to ${isChecked}`);
    // new checked state not applied to input element
    this.setState({
      [name]: isChecked,
      [field]: isChecked ? 'NA' : ''
    });
  }

  private _onPlainCheckBoxChange(ev: any) {
    const target: HTMLInputElement = ev.target;
    const isChecked = target.checked;
    const name: string = target.name;
    const field: string = target.dataset.field;
    console.info(`set ${name} checked value to ${isChecked}`);
    this.setState({
      [name]: isChecked,
      [field]: isChecked ? 'NA' : ''
    });
  }

  public render(): React.ReactElement<IHelloWorldProps> {
    return (
      <div className={styles.formWrapper}>
        <div>

          <Checkbox name="naBroadcastEntities" inputProps={{ 'data-field': 'broadcastEntities' } as any}
            checked={this.state.naBroadcastEntities}
            onChange={this._onCheckBoxChange}
          />

          <TextField name="broadcastEntities" data-field="broadcastEntities"
            value={this.state.broadcastEntities}
            onChange={this._onTextBoxChange}
          />

        </div>
        <div><br/>

          <input name="naPlainBroadcastEntities" type="checkbox" data-field="plainBroadcastEntities"
            checked={this.state.naPlainBroadcastEntities}
            onChange={this._onPlainCheckBoxChange}>
          </input>

          <input name="plainBroadcastEntities" type="text"
            value={this.state.plainBroadcastEntities}
            onChange={this._onTextBoxChange}>
          </input>


        </div>
      </div>
    );
  }
}
